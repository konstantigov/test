import React from 'react'
import './styles/index.scss'
import Routing from '../03_pages'

const App = () => {
  return (
    <Routing/>
  )
}

export default App