import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './01_app/App';

const root = ReactDOM.createRoot(
  document.getElementById('root')
);


root.render(
    <App />
);