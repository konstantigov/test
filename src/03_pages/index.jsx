import React, { lazy } from 'react'
import { RouterProvider, createBrowserRouter } from 'react-router-dom';

const RegistrationPage = lazy(() => import("./registration"));


const router = createBrowserRouter([
    {
      path: "/",
      element: <RegistrationPage />,
    },
  ]);


const Routing = () => {
  return (
    <RouterProvider router={router} />
  )
}

export default Routing